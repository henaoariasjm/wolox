import { Component, OnInit, ChangeDetectorRef, AfterViewChecked } from '@angular/core';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit, AfterViewChecked {

  constructor(private ref: ChangeDetectorRef) { }

  ngOnInit(): void {
  }

  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }

  /**
   * Valida si existe un usuario Loggeado
   */
  get isLogged(): boolean {
    return localStorage.getItem('token') !== null ? true : false;
  }

}
