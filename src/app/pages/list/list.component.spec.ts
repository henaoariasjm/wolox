import { TECHS, TECHS_FOR_FILTER, TECHS_ORDER_ASC, TECHS_ORDER_DESC, TECHS_FILTER_A, TECHS_FILTER_R, TECHS_FILTER_A_ORDER_ASC } from './../../shared/mocks/mock-data';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListComponent } from './list.component';

describe('ListComponent', () => {
  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;
  let httpMock: HttpTestingController;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListComponent);
    component = fixture.componentInstance;
    httpMock = TestBed.inject(HttpTestingController);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should GET All Techs', () => {
    const request = httpMock.expectOne('http://private-8e8921-woloxfrontendinverview.apiary-mock.com/techs');
    expect(request.request.method).toBe('GET');
    request.flush(TECHS);
  });

  it('should GET All Techs - ERROR', () => {
    const request = httpMock.expectOne('http://private-8e8921-woloxfrontendinverview.apiary-mock.com/techs');
    expect(request.request.method).toBe('GET');
    request.flush('404', { status: 404, statusText: 'Error' });
  });

  it('should Order by ASC', () => {
    component.typeOrder = 'desc';
    component.filteredData = TECHS;
    component.orderAscOrDesc();
    expect(component.filteredData).toBe(TECHS_ORDER_ASC);
    expect(component.typeOrder).toEqual('asc');
  });

  it('should Order by DESC', () => {
    component.typeOrder = 'asc';
    component.filteredData = TECHS;
    component.orderAscOrDesc();
    expect(component.filteredData).toEqual(TECHS_ORDER_DESC);
    expect(component.typeOrder).not.toEqual('asc');
    expect(component.typeOrder).toEqual('desc');
  });

  it('should Filter with \'R\'', () => {
    component.technologies = TECHS_FOR_FILTER;
    component.searchValue = 'R';
    component.filter();
    expect(component.filteredData).not.toBe(TECHS_FOR_FILTER);
    expect(component.filteredData).toEqual(TECHS_FILTER_R);
  });

  it('should Filter with \'A\' after Filter ASC', () => {
    component.typeOrder = 'desc';
    component.filteredData = TECHS_FOR_FILTER;
    component.orderAscOrDesc();
    component.technologies = TECHS_FOR_FILTER;
    component.searchValue = 'A';
    component.filter();
    expect(component.filteredData).toEqual(TECHS_FILTER_A_ORDER_ASC);
  });

  it('should Filter with WHITE SPACE ', () => {
    component.technologies = TECHS_FOR_FILTER;
    component.searchValue = '';
    component.filter();
    expect(component.filteredData).toBe(TECHS_FOR_FILTER);
    expect(component.filteredData).not.toBe(TECHS_FILTER_A);
  });

  it('should Fav Technology', () => {
    localStorage.setItem('techs', '');
    component.fav('Angular');
    expect(component.favoritesTechs).toEqual(['Angular']);
  });

  it('should Favorite Technology delete', () => {
    const favTechs = ['Angular', 'Android'].toString();
    localStorage.setItem('techs', favTechs);
    component.fav('Angular');
    expect(component.favoritesTechs).not.toEqual(['Angular']);
    expect(component.favoritesTechs).toEqual(['Android']);
  });

  it('should check if Is Favorite', () => {
    const favTechs = ['Angular', 'Android', 'Springboot'].toString();
    localStorage.setItem('techs', favTechs);
    expect(component.isFav('Angular')).toBeTruthy();
  });

  it('should check if Is Not Favorite', () => {
    const favTechs = ['Angular', 'Android', 'Springboot'].toString();
    localStorage.setItem('techs', favTechs);
    expect(component.isFav('Node')).toBeFalsy();
  });

  it('should check if Is Favorite with Favorites Techs empty', () => {
    const favTechs = [].toString();
    localStorage.setItem('techs', favTechs);
    expect(component.isFav('React')).toBeFalsy();
  });
});
