import { AfterViewChecked, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Technology } from 'src/app/shared/models/Technology.model';
import { ApiService } from 'src/app/shared/services/api.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  providers: [ApiService]
})
export class ListComponent implements OnInit, AfterViewChecked {

  filteredData: Array<Technology> = []; // Lista de Tecnologías que se usuará en la plantilla
  typeOrder = ''; // Tipo de orden
  searchValue: string; // Valor buscado en el filtro
  private technologies: Array<Technology> = [];  // Lista de Tecnologías principal

  constructor(private serviceApi: ApiService, private ref: ChangeDetectorRef) { }

  /**
   * Obtiene la lista de Tecnologías de la API
   */
  private getListAllTechs(): void {
    this.serviceApi.listAllTechs().subscribe(success => {
      this.technologies = success;
      this.filteredData = this.technologies;
    }, error => {
      console.error('Error: ' + error);
    });
  }

  /**
   * Ordena la lista de Tecnologías de filteredData en el tipo de orden designado
   */
  orderAscOrDesc(): void {
    this.typeOrder = this.typeOrder === 'asc' ? 'desc' : 'asc';
    const returnA = this.typeOrder === 'asc' ? 1 : -1;
    const returnB = this.typeOrder === 'asc' ? -1 : 1;
    this.filteredData.sort((a, b) => a.tech > b.tech ? returnA : a.tech < b.tech ? returnB : 0);
  }

  /**
   * Filtra la lista de Tecnologías filteredData de acuerdo con los datos ingresados en el campo searchValue
   */
  filter(): void {
    if (this.searchValue !== undefined) {
      this.filteredData = this.searchValue === '' ? this.technologies :
        this.technologies.filter((item: Technology) => {
          return item.tech.toLowerCase().startsWith(this.searchValue.toLowerCase()) ||
            item.type.toLowerCase().startsWith(this.searchValue.toLowerCase());
        });
    }
  }

  /**
   * Agrega o elimina de favoritos a una Tecnología seleccionada
   * @param tech --> Nombre de tecnología
   */
  fav(tech: string): void {
    const favorite = this.favoritesTechs;
    const index = favorite.findIndex(x => x === tech);
    index === -1 ? favorite.push(tech) : favorite.splice(index, 1);
    localStorage.setItem('techs', favorite.toString());
  }

  /**
   * Valida si la Tecnología se encuentra en la lista de favoritas
   * @param tech --> Nombre de Tecnología
   */
  isFav(tech): boolean {
    if (this.favoritesTechs.length > 0) {
      return this.favoritesTechs.find(x => x === tech) !== undefined ? true : false;
    }
    return false;
  }

  /**
   * Obtiene la lista de Tecnologías favoritas guardadas en el LocalStorage
   */
  get favoritesTechs(): Array<string> {
    const data = localStorage.getItem('techs');
    return data !== null && data !== '' ? data.split(',') : [];
  }

  // Implementar el método `ngOnInit` de OnInit
  ngOnInit(): void {
    this.getListAllTechs();
  }

  // Implementar el método `ngAfterViewChecked` de AfterViewChecked
  ngAfterViewChecked(): void {
    this.filter();
    this.ref.detectChanges();
  }
}
