import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'register',
    loadChildren: () => import('../app/auth/register/register.module').then(m => m.RegisterModule)
  },
  {
    path: '',
    loadChildren: () => import('../app/pages/pages.module').then(m => m.PagesModule)
  },
  { path: '**', redirectTo: 'landing' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
